#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <fstream>

#include "driver.h"

class Lexer
{
public:
    explicit Lexer(std::istream& ifs_, size_t initSize = 1024);
    explicit Lexer(const std::string& str);
    ~Lexer();

    int fill(int n = 1024);

    std::string text();
    int length();
    int lineno();
    void increment_line_number();

    int scan(TokenValue& yylval);

private:
    // iostream sucks. very slow.
    std::istream* _ifs;

    // buffer memory
    char*  _buffer;
    size_t _bufferSize;

    // current position
    char* _cursor;
    char* _limit;
    char* _token;
    char* _marker;
    int _lineno;
};

#endif // LEXER_H
