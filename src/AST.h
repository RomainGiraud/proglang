#ifndef AST_H
#define AST_H

#include <iostream>
#include <string>
#include <algorithm>
#include <variant>

class Node
{
public:
    Node() {}
    virtual ~Node() {}

    virtual double eval() const = 0;
    virtual std::ostream& print(std::ostream &os) const = 0;
};

using node_list = std::list<Node*>;

// temporary
static std::ostream& operator<< (std::ostream &os, const Node& node)
{
    return node.print(os);
}

class Number : public Node
{
public:
    Number(double d)
        : value(d) {}
    virtual ~Number() {}

    virtual double eval() const
    {
        return value;
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << value;
        return os;
    }

protected:
    double value;
};

class Variable : public Node
{
public:
    Variable(const std::string &d)
        : name(d) {}
    virtual ~Variable() {}

    virtual double eval() const
    {
        return 0;
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << name;
        return os;
    }

protected:
    std::string name;
};

class Assignment : public Node
{
public:
    Assignment(Variable *var, Node *node)
        : var(var), node(node) {}
    virtual ~Assignment() {}

    virtual double eval() const
    {
        return 0;
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << *var << " = " << *node;
        return os;
    }

protected:
    Variable *var;
    Node *node;
};

class UnaryOperation : public Node
{
public:
    UnaryOperation(Node *child)
        : child(child) {}
    virtual ~UnaryOperation()
    {
        if (child != 0)
            delete child;
    }

protected:
    Node *child;
};

class Negative : public UnaryOperation
{
public:
    Negative(Node *child)
        : UnaryOperation(child) {}
    virtual ~Negative() {}

    virtual double eval() const
    {
        return - child->eval();
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << '-' << child;
        return os;
    }
};

class BinaryOperation : public Node
{
public:
    BinaryOperation(Node *left, Node *right)
        : left(left), right(right) {}
    virtual ~BinaryOperation()
    {
        if (left != 0)
            delete left;
        if (right != 0)
            delete right;
    }

protected:
    Node *left;
    Node *right;
};

class Addition : public BinaryOperation
{
public:
    Addition(Node *left, Node *right)
        : BinaryOperation(left, right) {}
    virtual ~Addition() {}

    virtual double eval() const
    {
        return left->eval() + right->eval();
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << '(' << *left << " + " << *right << ')';
        return os;
    }
};

class Substraction : public BinaryOperation
{
public:
    Substraction(Node *left, Node *right)
        : BinaryOperation(left, right) {}
    virtual ~Substraction() {}

    virtual double eval() const
    {
        return left->eval() - right->eval();
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << '(' << *left << " - " << *right << ')';
        return os;
    }
};

class Multiplication : public BinaryOperation
{
public:
    Multiplication(Node *left, Node *right)
        : BinaryOperation(left, right) {}
    virtual ~Multiplication() {}

    virtual double eval() const
    {
        return left->eval() * right->eval();
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << '(' << *left << " * " << *right << ')';
        return os;
    }
};

class Division : public BinaryOperation
{
public:
    Division(Node *left, Node *right)
        : BinaryOperation(left, right) {}
    virtual ~Division() {}

    virtual double eval() const
    {
        return left->eval() / right->eval();
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << '(' << *left << " / " << *right << ')';
        return os;
    }
};

class Function : public Node
{
public:
    Function(const std::string& name)
        : name(name) {}
    virtual ~Function() {}

    virtual double eval() const
    {
        return 0;
    }

    // TODO remove this method
    virtual std::ostream& print(std::ostream &os) const
    {
        std::cout << "Function" << std::endl;
        os << "name()";
        return os;
    }

protected:
    std::string name;
};

template <typename ...T>
class FunctionPrint : public Function
{
public:
    FunctionPrint(node_list* args)
        : Function("print"), arguments(args) {}
    virtual ~FunctionPrint() {}

    virtual double eval() const
    {
        std::for_each(arguments->begin(), arguments->end(),
            [](auto&& a){
                using mT = std::decay_t<decltype(a)>;
                if constexpr (std::is_pointer_v<mT>)
                    std::cout << *a << ' ';
                else
                    std::cout << a << ' ';
            });

        return 0;
    }

    virtual std::ostream& print(std::ostream &os) const
    {
        os << name << "()";
        return os;
    }

protected:
    node_list* arguments;
};

#endif // AST_H
