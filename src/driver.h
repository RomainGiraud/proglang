#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <list>

struct TokenValue {
    char* str;
    size_t str_len;
    int numberi;
    float numberf;
    bool boolean;
};

class Node;
struct ParserState {
    std::list<Node*>* statements;
    bool error_found;

    ParserState();
    ~ParserState();
};

void* ParseAlloc(void* (*allocProc)(size_t));
void Parse(void* parser, int token, TokenValue value, ParserState* state);
void ParseFree(void*, void(*freeProc)(void*));
void ParseTrace(FILE *stream, char *zPrefix);

#endif // DRIVER_H
