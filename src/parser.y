%include
{
#include <cassert>
#include <iostream>
#include <list>
#include <map>

#include "driver.h"
#include "AST.h"

using namespace std;

}

%syntax_error
{
    std::cerr << "Error parsing command" << std::endl;
    state->error_found = true;
}

%parse_failure {
    std::cerr << "Parser is hopelessly lost..." << std::endl;
    state->error_found = true;
}

%stack_overflow {
    std::cerr << "Parser stack overflow" << std::endl;
    state->error_found = true;
}

%extra_argument { ParserState* state }

%token_prefix   TOKEN_
%token_type     { TokenValue }
%type exp               { Node* }
%type statement         { Node* }
%type statement_list    { node_list* }
%type function_call     { Node* }
%type argument_list     { node_list* }
%type argument          { Node* }

%left ADD SUB .
%left MUL DIV .

start ::= statement_list(l) .
{
    state->statements = l;
}

statement_list(l) ::= statement(st) .
{
    l = new node_list();
    l->push_back(st);
}
statement_list(l) ::= statement_list(ll) statement(st) .
{
    l = ll;
    l->push_back(st);
}

statement(st) ::= exp(e) SEMICOLON .
{
    st = e;
}
statement(st) ::= IDENTIFIER(id) ASSIGN exp(e) SEMICOLON .
{
    //std::cout << "assign" << std::endl;
    st = new Assignment(new Variable(std::string(id.str, id.str_len)), e);
}
statement(st) ::= function_call(fc) .
{
    //std::cout << "function" << std::endl;
    st = fc;
}

function_call(fc) ::= IDENTIFIER(id) OP CP SEMICOLON .
{
    //cout << "function: " << fn->str << endl;
    std::string fn (id.str, id.str_len);
    fc = new Function(fn);
}
function_call(fc) ::= IDENTIFIER(id) OP argument_list(al) CP SEMICOLON .
{
    std::string fn (id.str, id.str_len);
    //cout << "function: " << id << ", args: " << al->size() << endl;
    if (fn == std::string("print")) {
        fc = new FunctionPrint(al);
    } else {
        fc = new Function(fn);
    }
}
argument_list(al) ::= argument(a) .
{
    al = new node_list();
    al->push_back(a);
}
argument_list(al) ::= argument_list(l2) COMMA argument(a) .
{
    al = l2;
    al->push_back(a);
}
argument(a) ::= exp(e) .
{
    a = e;
}

exp(e) ::= exp(n) ADD exp(m) .
{
    e = new Addition(n, m);
}
exp(e) ::= exp(n) SUB exp(m) .
{
    e = new Substraction(n, m);
}
exp(e) ::= exp(n) MUL exp(m) .
{
    e = new Multiplication(n, m);
}
exp(e) ::= exp(n) DIV exp(m) .
{
    e = new Division(n, m);
}
exp(e) ::= OP exp(n) CP .
{
    e = n;
}
exp(e) ::= NUMBER_INT(n) .
{
    e = new Number(n.numberi);
}
exp(e) ::= IDENTIFIER(id) .
{
    e = new Variable(std::string(id.str, id.str_len));
    //e = variables[v->str];
}