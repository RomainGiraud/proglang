#include "driver.h"

#include "AST.h"

ParserState::ParserState()
    : statements(nullptr), error_found(false)
{}

ParserState::~ParserState()
{
    for (const auto &e : *statements)
    {
        delete e;
    }
    delete statements;
    statements = 0;
}