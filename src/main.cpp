#include <iostream>
#include <fstream>

#include "lexer.h"
#include "AST.h"

using namespace std;

void parse(const std::string &filename)
{
    ifstream file(filename);
    if (!file.is_open())
    {
        throw runtime_error("error during opening file " + filename);
    }

    // Set up the lexer
    TokenValue token;
    Lexer lexer(file);

    // Set up the parser
    ParserState state;
    void* shellParser = ParseAlloc(malloc);

    int lexCode;
    while (true)
    {
        lexCode = lexer.scan(token);
        cout << "lex: " << lexCode << endl;

        if (lexCode < 0) // token not recognize
            break;

        Parse(shellParser, lexCode, token, &state);

        if (state.error_found) // error
            break;

        if (lexCode == 0) // EOF
            break;
    }

    try
    {
        if (lexCode < 0)
        {
            //delete state.statements;
            // state.statements = nullptr;
            // cout <<  lexer.lineno() << endl;
            throw runtime_error("The lexer encountered an error.");
            //cerr << "The lexer encountered an error at line " << yyget_lineno(lexer) << " and column " << yyget_column(lexer) << "." << endl;
        }

        if (state.error_found)
        {
            //delete state.statements;
            // state.statements = nullptr;
            // cout << lexer.lineno() << endl;
            throw runtime_error("The parser encountered an error.");
            //cerr << "The parser encountered an error at line " << yyget_lineno(lexer) << " and column " << yyget_column(lexer) << "." << endl;
        }

        for (const auto &e : *state.statements)
        {
            e->eval();
        }
    }
    catch (const std::runtime_error &e)
    {
        cerr << "ERROR: " << e.what() << endl;
    }

    // Cleanup the lexer and parser
    file.close();
    ParseFree(shellParser, free);
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " filename" << endl;
        return 1;
    }

    bool trace = false;
	if (trace)
		ParseTrace(stdout, (char*)"");
	else
		ParseTrace(0, 0);

    parse(argv[1]);

    return 0;
}
