#include "lexer.h"

#include <iostream>
#include <cstring>

#include "parser.h"

using namespace std;

/*!max:re2c*/

Lexer::Lexer(std::istream& ifs, size_t initSize)
    : _ifs(&ifs)
    , _buffer(0)
    , _bufferSize(initSize)
    , _cursor(0)
    , _limit(0)
    , _token(0)
    , _marker(0)
    , _lineno(1)
{
    _buffer = new char[_bufferSize+1];
    memset(_buffer, 0, _bufferSize + 1);
    _cursor = _marker = _token = _limit = _buffer + _bufferSize;
}

Lexer::Lexer(const std::string& str)
    : _ifs(0)
    , _buffer(0)
    , _bufferSize(str.size() + YYMAXFILL)
    , _cursor(0)
    , _limit(0)
    , _token(0)
    , _marker(0)
    , _lineno(1)
{
    _buffer = new char[_bufferSize];
    memcpy(_buffer, str.c_str(), str.size());
    memset(_buffer + str.size(), ' ', YYMAXFILL);

    _cursor = _buffer;
    _token  = _buffer;
    _marker = _buffer;
    _limit  = _buffer + _bufferSize;
}

Lexer::~Lexer()
{
    delete[] _buffer;
    _cursor = _marker = _token = _limit = _buffer = 0;
}

int Lexer::fill(int n)
{
    if (_ifs->eof())
    {
        return 1;
    }

    const size_t free = _token - _buffer;
    if (free < 1) {
        cerr << "buffer full" << endl;
        return 2;
    }

    memmove(_buffer, _token, _limit - _token);
    _limit -= free;
    _cursor -= free;
    _marker -= free;
    _token -= free;

    _ifs->read(_limit, free);
    _limit += _ifs->gcount();
    _limit[0] = 0;

    return 0;
}

std::string Lexer::text()
{
    return std::string(_token, length());
}

int Lexer::length()
{
    return (_cursor - _token);
}

int Lexer::lineno()
{
    return _lineno;
}

void Lexer::increment_line_number()
{
    _lineno++;
}

int Lexer::scan(TokenValue& yylval)
{
std:
    _token = _cursor;

/*!re2c
    re2c:eof = 0;
    re2c:define:YYCTYPE = char;
    re2c:define:YYCURSOR = _cursor;
    re2c:define:YYMARKER = _marker;
    re2c:define:YYLIMIT = _limit;
    re2c:define:YYFILL = "fill";
    re2c:indent:top = 2;
    re2c:indent:string="    ";

    quote       = "\\" "\"";
    solidus     = "\\" "\\";
    rsolidus    = "\\" "/";
    backspace   = "\\" "b";
    formfeed    = "\\" "f";
    newline     = "\\" "n";
    return      = "\\" "r";
    tab         = "\\" "t";
    unicode     = "\\" [0-9a-fA-F]{4};
    character   = [^"\\] | quote | solidus | rsolidus | backspace | formfeed | newline | return | tab | unicode;

    ident       = [a-zA-Z_][a-zA-Z0-9_]*;
    int         = "-"? ("0"|[1-9][0-9]*);
    float       = int "." [0-9]+ ( [eE] [+-]? [0-9]+ )?;


    "("                     { return TOKEN_OP; }
    ")"                     { return TOKEN_CP; }

    ";"                     { return TOKEN_SEMICOLON; }
    ","                     { return TOKEN_COMMA; }

    "+"                     { return TOKEN_ADD; }
    "-"                     { return TOKEN_SUB; }
    "*"                     { return TOKEN_MUL; }
    "/"                     { return TOKEN_DIV; }
    "="                     { return TOKEN_ASSIGN; }

    ident                   { yylval.str_len = length(); yylval.str = _token; return TOKEN_IDENTIFIER; }
    int                     { yylval.numberi = std::atoi(_token); return TOKEN_NUMBER_INT; }

    "\n"                    { increment_line_number(); goto std; }
    [ \r\t\f]               { goto std; }

    *                       { return -1; }
    $                       { return 0; }
*/

/*
    "{"                     { return TOKEN_BRACE_OPEN; }
    "}"                     { return TOKEN_BRACE_CLOSE; }

    "["                     { return TOKEN_BRACKET_OPEN; }
    "]"                     { return TOKEN_BRACKET_CLOSE; }

    ","                     { return TOKEN_COMMA; }
    ";"                     { return TOKEN_SEMICOLON; }

    "true"                  { yylval.boolean = true; return TOKEN_BOOLEAN; }
    "false"                 { yylval.boolean = false; return TOKEN_BOOLEAN; }
    "null"                  { return TOKEN_NULL; }

    "\"" character* "\""    { yylval.str_len = length(); yylval.str = strndup(_token, yylval.str_len); return TOKEN_STRING; }
    int                     { yylval.numberi = std::atoi(_token); return TOKEN_NUMBER_INT; }
    float                   { yylval.numberf = std::atof(_token); return TOKEN_NUMBER_FLOAT; }

    [ \r\n\t\f]             { goto std; }
    .                       { cerr << "[error] unknown character: " << text() << endl; return -1; }
*/
}
